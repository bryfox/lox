use std::sync::atomic::{AtomicBool, Ordering};

static HAD_ERROR: AtomicBool = AtomicBool::new(false);

pub fn had_error() -> bool {
    HAD_ERROR.load(Ordering::SeqCst)
}

pub fn error(line: u64, message: &str) {
    mark_error();
    report(line, "", message);
}

pub fn report(line: u64, location: &str, message: &str) {
    println!("[line {}] Error{}: {}", line, location, message);
}

pub fn clear() {
    HAD_ERROR.compare_exchange(
        true,
        false,
        Ordering::SeqCst,
        Ordering::Relaxed
    );
}

fn mark_error() {
    HAD_ERROR.compare_exchange(
        false,
        true,
        Ordering::SeqCst,
        Ordering::Relaxed
    );
}
