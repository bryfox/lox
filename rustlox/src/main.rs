use std::{env, io};
use std::fs;
use std::process::exit;

mod scanner;
mod error;

fn main() {
    ctrlc::set_handler(move || { exit(0); })
        .expect("Error setting handler");

    let arguments: Vec<String> = env::args().collect();
    let args = &arguments[1..arguments.len()];
    if args.len() > 1 {
        println!("Usage: jlox [script]");
        exit(64);
    }
    if args.len() == 1 {
        run_file(&args[0]);
    } else {
        run_prompt();
    }
}

fn run_file(filename: &str) {
    let script = fs::read_to_string(filename)
        .expect("Could not open script");
    run(&script);

    if error::had_error() {
        exit(65);
    }
}

fn run_prompt() {
    let mut input = String::new();

    loop {
        io::stdin()
            .read_line(&mut input)
            .expect("Input failed");

        if !input.is_empty() {
            run(&input);
        } else {
            exit(0);
        }

        input.clear();
        error::clear();
    }
}

fn run(source :&str) {
    let tokens = scanner::scan_tokens(source);
    for token in tokens {
        println!("- {}", token);
    }
}

